<?php
namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Command\UserPasswordEncoderCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\RegistrationType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Nelmio\CorsBundle\NelmioCorsBundle;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

header("Access-Control-Allow-Origin: *");




class UserController extends Controller
{

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
        {
            $this->passwordEncoder=$passwordEncoder;
        }
    
    /**
     * @Route("/inscription", name="security_registration", methods={"POST"})
     */
    
    
    public function registration(Request $request, UserPasswordEncoderInterface $encoder , TokenStorageInterface $tokenStorage)
        {
            $user = new User();
            $username               = $request->request->get("username");
            $email                  = $request->request->get("email");
            $password               = $request->request->get("password");
    

            $errors = [];
            $encodedPassword = $encoder->encodePassword($user, $password);
            if(!$errors)
            {
                $encodedPassword = $encoder->encodePassword($user, $password);
                $user->setEmail($email);
                $user->setUsername($username);
                $user->setPassword($encodedPassword);
                $user->setRole("ROLE_USER");
    
                try
                {
                    $em=$this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();


                    $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
                    $tokenStorage->setToken($token);
        
                    $this->addFlash('success', 'You have been successfully registered! Congratulations');
    
                       return $this->json([
                           'user' => $user,
                           'token' => $token
                       ]);
                }
                catch(UniqueConstraintViolationException $e)
                {
                    $errors[] = "The email provided already has an account!";
                    dd($e->getMessage());
    
                }
                catch(\Exception $e)
                {
                    $errors[] = "Unable to save new user at this time.";
                    dd($e->getMessage());
    
                }
    
            }
            return $this->json([
                'errors' => $errors
            ], 400);


    
        }


        /**
         * @Route("/login", name="security_login", methods={"POST"})
         */
        
    public function loginuser (Request $request , UserRepository $userRepository)
           
        {
                $data = json_decode($request->getContent(), true) ?: [];
                $user = $userRepository->findOneByEmail($data['email']);
                $token = $this->get('lexik_jwt_authentication.encoder')
                                         ->encode(['email' => $data['email']]);

                        return $this->json(['data' => ['token' => $token, 'user' => $user]], 200);


        }
        }

        class PreflightIgnoreOnNewRelicListener
        {
            public function onKernelResponse(FilterResponseEvent $event)
            {
                if (!extension_loaded('newrelic')) {
                    return;
                }
        
                if ('OPTIONS' === $event->getRequest()->getMethod()) {
                    newrelic_ignore_transaction();
                }
            }
        }
    